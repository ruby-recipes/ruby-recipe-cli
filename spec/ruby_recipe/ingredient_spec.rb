# frozen_string_literal: true

require 'ruby_recipe/ingredient'

module RubyRecipe
  describe Ingredient do
    before do
      @ingredient = Ingredient.new 'steak', 1, 'pounds'
    end
    context 'can alter ingredients' do
      it 'can change ingredient name' do
        @ingredient.name = 'cheese'
        expect(@ingredient.name).to eq 'cheese'
      end
      it 'can change number' do
        @ingredient.number = '56'
        expect(@ingredient.number).to eq '56'
      end
      it 'can change unit name' do
        @ingredient.unit = 'ounces'
        expect(@ingredient.unit).to eq 'ounces'
      end
    end
    context 'raises type errors' do
      it 'raises a type error if name is a number' do
        expect { Ingredient.new(1, 4, nil) }.to raise_error TypeError
      end
      it 'raises a type error if number is string' do
        expect { Ingredient.new('string cheese', 'cat', nil) }.to raise_error TypeError
      end
      it 'raises a type error if unit is int' do
        expect { Ingredient.new('string cheese', 1, 4) }.to raise_error TypeError
      end
    end
  end
end
