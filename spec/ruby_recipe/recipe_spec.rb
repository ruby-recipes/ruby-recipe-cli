# frozen_string_literal: true

require 'ruby_recipe/recipe'
require 'ruby_recipe/ingredient'

module RubyRecipe
  describe Recipe do
    before do
      $stdout = StringIO.new
      @recipe = Recipe.new(
        [Ingredient.new('steak', 1),
         Ingredient.new('cheese', 2)], ['cook steak', 'add cheese']
      )
    end
    def build_ingredients_list(recipe)
      ingredients = []
      recipe.ingredients.each do |ingredient|
        ingredients.append ingredient.name
      end
      ingredients
    end
    context 'works with ingredients' do
      it 'can list ingredients with as a hash'
      it 'increments quantity by n if same ingredient is added' do
        ingredient_to_add = Ingredient.new('steak', 3)
        new_number_of_ingredient_to_add = ingredient_to_add.number
        if @recipe.ingredient_exists? ingredient_to_add.name
          existing_ingredient = @recipe.ingredients[@recipe.find_ingredient(ingredient_to_add.name)]
          existing_amount = existing_ingredient.number
          @recipe.add_ingredient ingredient_to_add
          expect(existing_ingredient.number).to(
            eq new_number_of_ingredient_to_add + existing_amount
          )
        end
      end
      it 'can add ingredients' do
        new_ingredient = 'sour cream'.upcase
        @recipe.add_ingredient(Ingredient.new(new_ingredient, 2))
        expect(build_ingredients_list(@recipe).include?(new_ingredient)).to be_truthy
      end
      it 'can determine when an ingredient exists' do
        new_ingredient = 'steak'.upcase
        expect(@recipe.ingredient_exists?(new_ingredient)).to be_truthy
      end
      it 'can determine when an ingredient does not exist' do
        expect(@recipe.ingredient_exists?('fish')).to be_falsey
      end
      it 'can remove an ingredient, if it exists' do
        ingredient_to_remove = 'cheese'.upcase
        if @recipe.ingredient_exists? ingredient_to_remove
          @recipe.remove_ingredient(ingredient_to_remove)
          ingredients = build_ingredients_list @recipe
          expect(ingredients.include?(ingredient_to_remove)).to be_falsey
        end
      end
      it 'cannot remove an ingredient that does not exist' do
        expect { @recipe.remove_ingredient('egg') }.to raise_error RecipeError
      end
    end
    context 'works with steps' do
      it 'can add a step' do
        @recipe.add_step 'boil chicken'
        expect(@recipe.steps.include?('boil chicken')).to be_truthy
      end
      it 'can remove a step' do
        @recipe.remove_step 'cook steak'
        expect(@recipe.steps.include?('cook steak')).to be_falsey
      end
      it 'cannot remove a step that does not exist' do
        expect { @recipe.remove_step('boil steak') }.to raise_error RecipeError
      end
    end
  end
end
