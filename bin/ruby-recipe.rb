#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/ruby_recipe/recipe'
require_relative '../lib/ruby_recipe/ingredient'

ingredients = [RubyRecipe::Ingredient.new('steak', 1), RubyRecipe::Ingredient.new('beans', 3)]
recipe = RubyRecipe::Recipe.new(ingredients, ['boil water'])
recipe.add_ingredient RubyRecipe::Ingredient.new 'rice', 4, 'pounds'
recipe.remove_ingredient 'BEANS'
recipe.add_step 'make dinner'
puts recipe
