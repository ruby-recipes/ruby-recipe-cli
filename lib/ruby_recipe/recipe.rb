# frozen_string_literal: true

require_relative 'ingredient'

module RubyRecipe
  # Builds Recipes with Ingriedents
  class Recipe
    attr_reader :ingredients, :steps

    def initialize(ingredients = [], steps = [])
      raise TypeError unless (ingredients.is_a?(Ingredient) ||
        ingredients.is_a?(Array)) && steps.is_a?(Array)

      @ingredients = ingredients
      @steps = steps
    end

    def list_ingredients
      ingredients = []
      @ingredients.each do |ingredient|
        ingredients.append ingredient.name
      end
      ingredients
    end

    def find_ingredient(ingredient)
      list_ingredients.find_index(ingredient)
    end

    def add_ingredient(ingredient)
      ingredient.name = ingredient.name.upcase
      raise(TypeError, 'Must be an Ingredient object') unless ingredient.is_a? Ingredient

      if ingredient_exists?(ingredient.name)
        @ingredients[list_ingredients.find_index(ingredient.name)].number +=
          ingredient.number
      else
        @ingredients.append(ingredient)
      end
    end

    def ingredient_exists?(ingredient)
      list_ingredients.include? ingredient.upcase
    end

    def remove_ingredient(ingredient_to_remove)
      ingredient_to_remove = ingredient_to_remove.upcase
      string_value_error ingredient_to_remove
      raise(RecipeError, 'Ingredient not in ingredients list') unless ingredient_exists?(ingredient_to_remove)

      @ingredients.delete_at(list_ingredients.find_index(ingredient_to_remove))
    end

    def add_step(step)
      string_value_error step
      @steps.append step
    end

    def remove_step(step)
      string_value_error step
      raise(RecipeError, 'Step not in steps list') unless @steps.include?(step)

      @steps.delete_at(@steps.index(step))
    end

    def string_value_error(var)
      raise(TypeError, 'Must be a String object') unless var.is_a? String
    end

    def to_s
      @ingredients.each do |ingredient|
        puts ingredient.name
      end
      @steps.each do |step|
        puts step.to_s
      end
    end
  end
end

class RecipeError < StandardError
end
