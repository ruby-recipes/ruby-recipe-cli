# frozen_string_literal: true

module RubyRecipe
  # Creates Ingredients for use by Recipe and ShoppingList classes.
  class Ingredient
    attr_accessor :number, :name, :unit

    def initialize(name, number = 1, unit = nil)
      raise TypeError unless name.is_a?(String) &&
                             (unit.is_a?(String) || unit.is_a?(NilClass)) &&
                             (number.is_a?(Integer) || number.is_a?(Float))

      @name = name.upcase
      @number = number
      @unit = unit
    end
  end
end
