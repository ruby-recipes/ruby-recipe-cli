FROM ruby:3.0.4
COPY ruby-recipes.rb /home
WORKDIR /home
CMD ["ruby", "ruby-recipes.rb"]
