Gem::Specification.new do |s|
    s.name         = "RubyRecipe"
    s.version      = "0.5"
    s.author       = "Edward Hilgendorf"
    s.email        = "edward@hilgendorf.me"
    s.homepage     = "https://cacheheadbiscuit.io/ruby-recipe"
    s.summary      = "Recipe and ingredient tracking"
    s.description  = File.read(File.join(File.dirname(__FILE__), 'README'))
    s.licenses     = ['MIT']
  
    s.files         = Dir["{bin,lib,spec}/**/*"] + %w(LICENSE README)
    s.test_files    = Dir["spec/**/*"]
    s.executables   = [ 'ruby-recipe.rb' ]
  
    s.required_ruby_version = '>=3.0'
    s.add_development_dependency 'rspec', '~> 2.8', '>= 2.8.0'
  end
  
